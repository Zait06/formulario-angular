import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface Usuario{
  nombre:string;
  correo:string;
  password:string;
}

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit {
  formularioCreado:FormGroup;
  esNuevo:boolean=true;
  posEdit:number=-1;
  usuarios:Array<Usuario>=new Array<Usuario>();

  constructor(private formBuilder:FormBuilder) { }

  ngOnInit() {
    this.crearFormulario();
  }

  crearFormulario(){
    this.formularioCreado=this.formBuilder.group({
      nombre:['Carmen',Validators.required],
      correo:['',Validators.compose([
        Validators.required, Validators.email
      ])],
      password:['',Validators.compose([
        Validators.required, Validators.minLength(8)
      ])]
    });
  }

  agregar(){
    this.usuarios.push(this.formularioCreado.value as Usuario);
    this.formularioCreado.reset();
  }

  editar(){
    this.usuarios[this.posEdit].nombre=this.formularioCreado.value.nombre;
    this.usuarios[this.posEdit].correo=this.formularioCreado.value.correo;
    this.usuarios[this.posEdit].password=this.formularioCreado.value.password;
    this.formularioCreado.reset();
    this.posEdit=-1;
    this.esNuevo=true;
  }

  editarUsuario(pos:number){
    this.formularioCreado.setValue({
      nombre:this.usuarios[pos].nombre,
      correo:this.usuarios[pos].correo,
      password:this.usuarios[pos].password
    });
    this.posEdit=pos;
    this.esNuevo=false;
  }

  eliminarUsuario(pos:number){
    this.usuarios.splice(pos,1);
  }

}
